/*
 * This file is part of gtkD.
 *
 * gtkD is free software; you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2.1 of the License, or
 * (at your option) any later version.
 *
 * gtkD is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with gtkD; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
 
// generated automatically - do not change
// find conversion definition on APILookup.txt
// implement new conversion functionalities on the wrap.utils pakage

/*
 * Conversion parameters:
 * inFile  = gdk-Cairo-Interaction.html
 * outPack = gdk
 * outFile = GdkCairo
 * strct   = 
 * realStrct=
 * ctorStrct=
 * clss    = 
 * interf  = 
 * class Code: Yes
 * interface Code: No
 * template for:
 * extend  = 
 * implements:
 * prefixes:
 * 	- gdk_cairo_
 * omit structs:
 * omit prefixes:
 * omit code:
 * 	- gdk_cairo_create
 * 	- gdk_cairo_set_source_color
 * 	- gdk_cairo_set_source_pixbuf
 * 	- gdk_cairo_set_source_pixmap
 * 	- gdk_cairo_rectangle
 * 	- gdk_cairo_region
 * imports:
 * 	- cairoLib.Cairo
 * structWrap:
 * 	- GdkColor* -> Color
 * 	- GdkDrawable* -> Drawable
 * 	- GdkPixbuf* -> Pixbuf
 * 	- GdkPixmap* -> Pixmap
 * 	- GdkRectangle* -> Rectangle
 * 	- GdkRegion* -> Region
 * 	- cairo_t* -> Cairo
 * module aliases:
 * local aliases:
 */

module gdk.GdkCairo;

version(noAssert)
{
	version(Tango)
	{
		import tango.io.Stdout;	// use the tango loging?
	}
}

private import gtkc.gdktypes;

private import gtkc.gdk;


private import cairoLib.Cairo;




/**
 * Description
 * Cairo is a graphics
 * library that supports vector graphics and image compositing that
 * can be used with GDK. Since 2.8, GTK+ does most of its drawing
 * using Cairo.
 * GDK does not wrap the Cairo API, instead it allows to create Cairo
 * contexts which can be used to draw on GDK drawables. Additional
 * functions allow to convert GDK's rectangles and regions into
 * Cairo paths and to use pixbufs as sources for drawing operations.
 */

/**
 * This file is not used,
 *
 * It is here just to tell you to look at
 * cairoLib.Cairo class for the methods that where here..
 */


/**
 */







