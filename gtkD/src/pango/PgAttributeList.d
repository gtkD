/*
 * This file is part of gtkD.
 *
 * gtkD is free software; you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2.1 of the License, or
 * (at your option) any later version.
 *
 * gtkD is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with gtkD; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
 
// generated automatically - do not change
// find conversion definition on APILookup.txt
// implement new conversion functionalities on the wrap.utils pakage

/*
 * Conversion parameters:
 * inFile  = 
 * outPack = pango
 * outFile = PgAttributeList
 * strct   = PangoAttrList
 * realStrct=
 * ctorStrct=
 * clss    = PgAttributeList
 * interf  = 
 * class Code: No
 * interface Code: No
 * template for:
 * extend  = 
 * implements:
 * prefixes:
 * 	- pango_attr_List_
 * omit structs:
 * omit prefixes:
 * 	- pango_attr_list_
 * omit code:
 * imports:
 * structWrap:
 * module aliases:
 * local aliases:
 */

module pango.PgAttributeList;

version(noAssert)
{
	version(Tango)
	{
		import tango.io.Stdout;	// use the tango loging?
	}
}

private import gtkc.pangotypes;

private import gtkc.pango;






/**
 * Description
 * Attributed text is used in a number of places in Pango. It
 * is used as the input to the itemization process and also when
 * creating a PangoLayout. The data types and functions in
 * this section are used to represent and manipulate sets
 * of attributes applied to a portion of text.
 */
public class PgAttributeList
{
	
	/** the main Gtk struct */
	protected PangoAttrList* pangoAttrList;
	
	
	public PangoAttrList* getPgAttributeListStruct()
	{
		return pangoAttrList;
	}
	
	
	/** the main Gtk struct as a void* */
	protected void* getStruct()
	{
		return cast(void*)pangoAttrList;
	}
	
	/**
	 * Sets our main struct and passes it to the parent class
	 */
	public this (PangoAttrList* pangoAttrList)
	{
		version(noAssert)
		{
			if ( pangoAttrList is null )
			{
				int zero = 0;
				version(Tango)
				{
					Stdout("struct pangoAttrList is null on constructor").newline;
				}
				else
				{
					printf("struct pangoAttrList is null on constructor");
				}
				zero = zero / zero;
			}
		}
		else
		{
			assert(pangoAttrList !is null, "struct pangoAttrList is null on constructor");
		}
		this.pangoAttrList = pangoAttrList;
	}
	
	/**
	 */
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
}
